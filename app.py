from flask import Flask
from flask import request
from flask import json

from kafka.kafka_producer import KafkaProducer


app = Flask(__name__)

# Global Configurations
# TODO: Move Global Configuration to separate files
kafka_config_dict = {
    'dev': '10.140.10.108:9092, 10.140.10.103:9092, 10.140.10.11:9092',
    'prod': '10.140.10.159:9092, 10.140.10.178:9092, 10.140.10.236:9092',
    'staging': '10.140.10.40:9092, 10.140.10.128:9092,10.140.10.204'
}
producer_config = {
    'bootstrap.servers': kafka_config_dict.get('dev', ''),
    'client.id': 'segmentio_logs_producer'
}
kafka_producer = KafkaProducer(**producer_config)


@app.route('/')
def entry_info():
    return 'Kafka Producer for SegmentIO Sink'


@app.route('/webhooks/analytics', methods=['POST'])
def segment_analytics_webhook():
    if request.headers['Content-Type'] == 'application/json':
        json_dump = json.dumps(request.json)
        kafka_producer.deliver_data_to_kafka_topic('segmentio-logs-sink-rewrite-prod', json_dump)
        return 'success'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
