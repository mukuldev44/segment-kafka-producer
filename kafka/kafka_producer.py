from confluent_kafka import Producer
from time import sleep


class KafkaProducer:
    def __init__(self, **config):
        self.producer = Producer(config)

    def delivery_report(self, err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            pass
            # print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

    def deliver_data_to_kafka_topic(self, topic, data):
        # Trigger any available delivery report callbacks from previous produce() calls
        self.producer.poll(0)
        sleep(0.0001)
        self.producer.produce(topic, data.encode('utf-8'), callback=self.delivery_report)
        self.producer.flush()
        # print(len(data))
        # Wait for any outstanding messages to be delivered and delivery report
        # callbacks to be triggered.
