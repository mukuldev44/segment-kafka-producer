# Segment-Kafka-Producer

## Deployment Steps

```

1. -- Install Python 3 --

    > sudo apt update
    > sudo apt install python3 -y
    > sudo apt install python3-pip -y


2. -- Clone `segment-kafka-producer` Repo --

    > git clone https://mukuldev44@bitbucket.org/mukuldev44/segment-kafka-producer.git
    > cd segment-kafka-producer


3. -- Install `segment-kafka-producer` -- 

    > pip3 install -r requirements.txt -y


4. -- Execute Flask App --

    > sudo python3 app.py
    
```
